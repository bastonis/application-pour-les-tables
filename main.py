""" Main file of the Bastonis Numeris application """

# -*- coding: utf-8 -*-
# @Author: brunomaxime
# @Date:   2022-10-22 22:51:27
# @Last Modified by:   brunomaxime
# @Last Modified time: 2022-10-22 23:09:21

__version__ = "0.1"

from kivymd.app import MDApp
from kivy.lang import Builder
# pylint: disable=E0611
from kivy.properties import ObjectProperty

import yaml

KV = """
Screen:
    in_class: text
    MDTextField:
        id: text
        hint_text: 'Entrer le petit numéro'
        pos_hint: {'center_x': 0.5, 'center_y': 0.05}
        size_hint_x: None
        required: True
        
    MDRectangleFlatButton:
        text: 'Submit'
        pos_hint: {'center_x': 0.5, 'center_y': 0.1}
        on_press:
            app.auth()
            
    MDLabel:
        text: ''
        id: show
        pos_hint: {'center_x': 0.5, 'center_y': 0.525}
"""


class Main(MDApp):
    """
    This class describes the main body of the application.
    """

    in_class = ObjectProperty(None)

    def build(self):
        return Builder.load_string(KV)

    def search(self):
        """
        Searches for the information related to the petit numéro.
        """
        with open("tables/petits_numeros.yml", 'r', encoding='utf-8') as file:
            content = yaml.safe_load(file)
        numero = int(self.root.in_class.text)
        if numero in content:
            label = self.root.ids.show
            label.text = yaml.dump(content[numero],
                                   allow_unicode=True,
                                   default_flow_style=False)
        else:
            label = self.root.ids.show
            label.text = "Petit numéro non existant"


Main().run()
