# Make bash as shell to allow the use of source
SHELL := /bin/bash

EXEC = bastonis_numerus

SOURCE = main.py

DEBUG = true

.PHONY: dep env share lint buildozer build deploy

all: deploy

venv: venv/touchfile

venv/touchfile: requirements.txt
	test -d venv || python -m venv venv
	. venv/bin/activate; python -m ensurepip; pip install -Ur requirements.txt
	touch venv/touchfile
    

lint: venv
	. venv/bin/activate; pip install pylint
	. venv/bin/activate; pylint -d C0301 $(SOURCE)


buildozer: venv
	. venv/bin/activate; pip install -U buildozer Cython==0.29.19


build: dep buildozer
ifeq ($(DEBUG), 'true')
	. venv/bin/activate; buildozer -v android debug
else
	. venv/bin/activate; buildozer -v android release
endif


deploy: build
	. venv/bin/activate; buildozer android deploy run logcat | grep python


share: build
	. venv/bin/activate; buildozer serve
